FROM rust:1.80.0 as rust-builder

RUN cargo install cargo-chef

FROM rust-builder as planner

WORKDIR /app
COPY . /app

RUN cargo chef prepare --recipe-path recipe.json

FROM rust-builder as build-env

RUN apt update && apt install -y libssl-dev pkg-config build-essential cmake

WORKDIR /app

COPY --from=planner /app/recipe.json recipe.json
RUN cargo chef cook --release --recipe-path recipe.json

COPY . /app

RUN cargo build --release --bin arti-controller

FROM ubuntu:24.04

RUN apt update && apt install -y ca-certificates && apt clean && rm -rf /var/lib/apt/lists/*
COPY --from=build-env /app/target/release/arti-controller /

CMD ["/arti-controller"]

