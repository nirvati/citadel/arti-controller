# Installing arti-controller

Arti-controller is provided as a Helm chart. To install it, make sure you have [Helm installed](https://helm.sh/docs/intro/install/), and then run:

```sh
helm repo add arti-controller https://arti-controller.nirvati.org/charts
helm install arti-controller arti-controller/arti-controller --create-namespace --namespace arti-controller
```

This will install the controller in the `arti-controller` namespace. You can then create an `OnionService` resource to create a new Onion Service.
