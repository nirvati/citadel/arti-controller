---
outline: deep
---

# Creating an onion service

::: info
Before creating an onion service, you need to have a running instance of the controller. If you haven't done that yet, please follow the [installation guide](/install).
:::

To create an onion service, you need to create an `OnionService` resource. Here is an example of an `OnionService` resource:

```yaml
apiVersion: arti.nirvati.org/v1alpha1
kind: OnionService
metadata:
  name: my-onion-service # The name of the onion service
  namespace: default # The namespace where the onion service should be created
spec:
  key_secret:
    name: onion-svc-keys # This secret will contain keys used for the onion service
    namespace: default # The namespace where the secret should be created (Optional, defaults to the onion service namespace)
  routes:
  - source_port: 80 # The port on which the service should listen on the onion service
    target:
      port: 80 # The port the request should be forwarded to
      svc: my-svc # The service to forward the request to
  # You can also forward the request to an arbitrary domain name.
  # Please note: Requests are forwarded at the TCP level.
  # HTTP/HTTPS requests are not handled specially, so the Host header will not be modified, and SNI will not be used.
  - source_port: 81
    target:
      dns_name: example.com 
      port: 80
  - source_port: 82
    target:
      ip: 127.0.0.1 # You can also forward the request to an arbitrary IP address
      port: 80
```
