---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "arti-controller"
  text: "Tor for Kubernetes"
  tagline: "A Kubernetes controller for running Onion Services"
  image:
    src: /logo.svg
    alt: arti-controller logo
  actions:
    - theme: brand
      text: Installation
      link: /install
    - theme: alt
      text: Creating services
      link: /examples

features:
  - icon: 📦
    title: Minimal
    details: Uses just one pod for the controller. No other pods need to be deployed.
  - icon: 🚀
    title: Fast
    details: The controller can respond to changes very quickly, without needing to restart Tor.
  - icon: 🦀
    title: Built on Rust & Arti
    details: This software is built on Top of Arti, a modern rewrite of Tor in Rust.

head:
  - - meta
    - property: og:type
      content: website
  - - meta
    - property: og:title
      content: arti-controller
  - - meta
    - property: og:image
      content: https://arti-controller.nirvati.org/og-image.png
  - - meta
    - property: og:url
      content: https://arti-controller.nirvati.org/
  - - meta
    - property: og:description
      content: Onion Services for Kubernetes
  - - meta
    - name: twitter:card
      content: summary_large_image
---


<style>
:root {
  --vp-home-hero-name-color: transparent;
  --vp-home-hero-name-background: -webkit-linear-gradient(120deg, #cb86ed 30%, #7e4798);

  --vp-home-hero-image-background-image: linear-gradient(-45deg, #cb86ed 50%, #7e4798 50%);
  --vp-home-hero-image-filter: blur(44px);
}

@media (min-width: 640px) {
  :root {
    --vp-home-hero-image-filter: blur(56px);
  }
}

@media (min-width: 960px) {
  :root {
    --vp-home-hero-image-filter: blur(68px);
  }
}

@media (min-width: 960px) {
    .VPHomeHero .container {
        padding: 5rem 0;
    }
}
</style>