# Arti controller

This is a Kubernetes controller for running Onion Services.

It runs a single pod in your cluster. Unlike most other Tor controllers, the arti-controller has Arti embedded in it.
This means that it can respond to changes in your Kubernetes cluster very quickly, without needing to restart Tor.

In addition, it just runs a single pod in your cluster for its functionality. No other pods need to be deployed.

Please note: Your network policies need to allow the namespace that the arti-controller is running in to connect to the internet,
and to any namespaces that you want to run Onion Services in.

[Learn more](https://arti-controller.nirvati.org/)
