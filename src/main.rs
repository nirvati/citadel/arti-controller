use arti_controller::controller::start_controller;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    tracing::debug!("Starting controller");
    start_controller().await.unwrap()
}
