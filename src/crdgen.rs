use kube::CustomResourceExt;

fn main() {
    print!(
        "{}",
        serde_yaml::to_string(&arti_controller::crd::OnionService::crd()).unwrap()
    )
}
