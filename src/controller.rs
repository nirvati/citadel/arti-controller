use crate::crd::OnionServiceStatus;
use crate::crd::{OnionService, OnionServiceRoute, OnionServiceTargetHost};
use crate::kube_secret_keystore::K8sSecretKeyStore;
use futures_util::stream::StreamExt;
use k8s_openapi::api::core::v1::Secret;
use kube::api::Patch;
use kube::error::ErrorResponse;
use kube::runtime::controller::Action;
use kube::runtime::finalizer::Event;
use kube::runtime::{finalizer, watcher, Controller};
use kube::{Api, Client, Error, Resource};
use serde_json::json;
use std::collections::BTreeMap;
use std::path::PathBuf;
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::Mutex;
use tokio::task::AbortHandle;
use tor_cell::relaycell::msg::{Connected, End, EndReason};
use tor_circmgr::hspool::HsCircPool;
use tor_dirmgr::DirMgr;
use tor_hsservice::config::OnionServiceConfigBuilder;
use tor_hsservice::{HsNickname, RunningOnionService};
use tor_keymgr::KeyMgr;
use tor_netdir::UpcastArcNetDirProvider;
use tor_proto::stream::IncomingStreamRequest;
use tor_rtcompat::PreferredRuntime;

struct RunningSvc {
    // This unused field is required to prevent the running service from being dropped
    svc: Arc<RunningOnionService>,
    key_mgr: Arc<KeyMgr>,
    secret: String,
    abort_handle: AbortHandle,
    routes: Vec<OnionServiceRoute>,
}

struct Context {
    client: Client,
    // NS -> svc_name -> svc
    running_svcs: BTreeMap<String, BTreeMap<String, RunningSvc>>,
    dirmgr: Arc<DirMgr<PreferredRuntime>>,
    circpool: Arc<HsCircPool<PreferredRuntime>>,
    state_dir: PathBuf,
}

/// The reconciler that will be called when either object change
async fn handle_update(g: Arc<OnionService>, ctx: Arc<Mutex<Context>>) -> Result<Action, Error> {
    let client = {
        let ctx = ctx.lock().await;
        let client = ctx.client.clone();
        drop(ctx);
        client
    };
    let Some(ns) = g.metadata.namespace.as_ref() else {
        tracing::debug!("No namespace found");
        return Err(Error::Api(ErrorResponse {
            status: "".to_string(),
            message: "".to_string(),
            reason: "".to_string(),
            code: 0,
        }));
    };
    let Some(name) = g.metadata.name.as_ref() else {
        tracing::debug!("No name found");
        return Err(Error::Api(ErrorResponse {
            status: "".to_string(),
            message: "".to_string(),
            reason: "".to_string(),
            code: 0,
        }));
    };
    let nickname = format!("{}-{}", ns, name);
    tracing::debug!("Doing running check!");
    let is_running = {
        let ctx = ctx.lock().await;
        ctx.running_svcs
            .get(ns)
            .and_then(|m| m.get(name))
            .is_some_and(|svc| svc.secret == g.spec.key_secret.name && svc.routes == g.spec.routes)
    };
    tracing::debug!("Did running check!");
    if is_running {
        tracing::debug!("Service already running");
        return Ok(Action::await_change());
    }
    let keystore = {
        tracing::debug!("Getting keystore!");
        let mut running_svc = ctx.lock().await;
        let running_svc = running_svc
            .running_svcs
            .entry(ns.clone())
            .or_default()
            .get_mut(name);
        let keystore = running_svc.as_ref().and_then(|svc| {
            if svc.secret == g.spec.key_secret.name {
                Some(svc.key_mgr.clone())
            } else {
                None
            }
        });
        keystore.unwrap_or_else(|| {
            let namespace = g.spec.key_secret.namespace.clone().unwrap_or(ns.clone());
            let new_keystore = K8sSecretKeyStore::new(namespace, g.spec.key_secret.name.clone())
                .expect("Failed to create keystore");
            let new_keymgr = tor_keymgr::KeyMgrBuilder::default()
                .primary_store(Box::new(new_keystore))
                .build()
                .unwrap();
            let arc = Arc::new(new_keymgr);
            if let Some(svc) = running_svc {
                svc.key_mgr = arc.clone();
            }
            arc
        })
    };
    let mut config = OnionServiceConfigBuilder::default();
    /*if g.spec.insecure_single_hop {
        config.anonymity(Anonymity::DangerouslyNonAnonymous);
    }*/
    let config = config
        .nickname(HsNickname::new(nickname.clone()).unwrap())
        .build()
        .unwrap();
    tracing::debug!("Starting onion service!");
    let state_dir_path = {
        let ctx = ctx.lock().await;
        ctx.state_dir.clone()
    };
    let state_dir = tor_persist::state_dir::StateDirectory::new(
        state_dir_path.clone(),
        &fs_mistrust::Mistrust::new_dangerously_trust_everyone(),
    )
    .unwrap();
    tracing::debug!("Preparing onion service!");
    let onion_svc = tor_hsservice::OnionServiceBuilder::default()
        .config(config)
        .state_dir(state_dir)
        .keymgr(keystore.clone())
        .build()
        .unwrap();
    tracing::debug!("Started onion service!");
    let rt_handle = PreferredRuntime::current().unwrap();
    {
        if let Some(running_svc) = ctx
            .lock()
            .await
            .running_svcs
            .get_mut(ns)
            .and_then(|m| m.remove(name))
        {
            running_svc.abort_handle.abort();
            drop(running_svc);
        }
    }
    // Wait for tmp_path/hss/{name}.lock to be deleted
    let lockfile = state_dir_path
        .join("hss")
        .join(format!("{}.lock", nickname));
    let mut counter = 0;
    while lockfile.exists() {
        tracing::warn!("Lockfile still exists for service {}", name);
        counter += 1;
        tokio::time::sleep(Duration::from_secs(1)).await;
        if counter > 10 {
            let _ = std::fs::remove_file(&lockfile);
        }
    }
    let (dirmgr, circpool) = {
        let ctx = ctx.lock().await;
        (ctx.dirmgr.clone(), ctx.circpool.clone())
    };
    let (running_onion_service, stream) = onion_svc
        // TODO: Can we just do this?
        .launch(
            rt_handle,
            dirmgr.upcast_arc(),
            circpool,
            Arc::new(tor_config_path::CfgPathResolver::default()),
        )
        .unwrap();
    let routes = g.spec.routes.clone();
    let ns_2 = ns.clone();
    let abort_handle = tokio::spawn(async move {
        tracing::debug!("Handling requests for onion service {}", nickname);
        let mut stream_requests = tor_hsservice::handle_rend_requests(stream).fuse();
        let routes = routes.clone();
        let ns = ns_2.clone();
        while let Some(req) = stream_requests.next().await {
            tracing::debug!("Got request");
            let routes = routes.clone();
            let ns = ns.clone();
            tokio::spawn(async move {
                tracing::debug!("Handling request");
                let incoming_req = req.request();
                let IncomingStreamRequest::Begin(begin) = incoming_req else {
                    return;
                };
                let port = begin.port();
                let route = routes.iter().find(|r| r.source_port == port);
                let Some(route) = route else {
                    tracing::debug!("Rejecting request because no route exists!");
                    let _ = req
                        .reject(End::new_with_reason(EndReason::CONNECTREFUSED))
                        .await;
                    return;
                };
                tracing::debug!("Handling request!");
                // Open a net stream to the target
                let target_host = match &route.target.host {
                    OnionServiceTargetHost::Service(svc) => {
                        // Format the dns name of the service
                        let ns = svc.ns.as_ref().cloned().unwrap_or(ns.clone());
                        format!("{}.{}.svc.cluster.local", svc.svc, ns)
                    }
                    OnionServiceTargetHost::DnsName(dns_name) => dns_name.dns_name.clone(),
                    OnionServiceTargetHost::Ip(ip_addr) => {
                        // Check if ip is a ipv4 or ipv6 address
                        if ip_addr.ip.contains(':') {
                            format!("[{}]", ip_addr.ip)
                        } else {
                            ip_addr.ip.clone()
                        }
                    }
                };
                let target = format!("{}:{}", target_host, route.target.port);
                let target_stream = tokio::net::TcpStream::connect(target).await;
                let Ok(mut target_stream) = target_stream else {
                    let _ = req
                        .reject(End::new_with_reason(EndReason::CONNECTREFUSED))
                        .await;
                    return;
                };
                let Ok(mut stream) = req.accept(Connected::new_empty()).await else {
                    return;
                };
                let _ = tokio::io::copy_bidirectional(&mut stream, &mut target_stream).await;
            });
        }
    })
    .abort_handle();
    let running_svc = RunningSvc {
        svc: running_onion_service.clone(),
        key_mgr: keystore,
        secret: g.spec.key_secret.name.clone(),
        abort_handle,
        routes: g.spec.routes.clone(),
    };
    {
        let mut ctx = ctx.lock().await;
        ctx.running_svcs
            .entry(ns.clone())
            .or_default()
            .insert(name.clone(), running_svc);
    }

    let onion_svc_name = running_onion_service.onion_name();

    // If this is set, patch the status of the object
    if let Some(svc_id) = onion_svc_name {
        tracing::debug!("Setting onion service name to {}", svc_id);
        let patch = json!({
            "status": OnionServiceStatus {
                onion_name: svc_id.to_string(),
                conditions: vec![]
            }
        });
        let api: Api<OnionService> = Api::namespaced(client, ns);
        api.patch_status(&name, &Default::default(), &Patch::Merge(patch))
            .await?;
        let client = api.into_client();
        if let Some(ref secret_for_name) = g.spec.store_name_in_secret {
            let namespace = secret_for_name.namespace.clone().unwrap_or(ns.clone());
            let secret_name = secret_for_name.name.clone();
            let key = secret_for_name.key.clone();
            let secret_api: Api<Secret> = Api::namespaced(client, &namespace);
            let secret = secret_api.get_opt(&secret_name).await?;
            if let Some(secret) = secret {
                let mut secret = secret;
                let data = secret.data.get_or_insert(Default::default());
                data.insert(
                    key,
                    k8s_openapi::ByteString(svc_id.to_string().as_bytes().to_vec()),
                );
                secret.metadata.managed_fields = None;
                secret_api
                    .patch(&secret_name, &Default::default(), &Patch::Merge(secret))
                    .await?;
            } else {
                tracing::warn!(
                    "Failed to get secret {} in namespace {}",
                    secret_name,
                    namespace
                );
            }
        }
    } else {
        tracing::warn!("Failed to get onion service name");
    }

    Ok(Action::await_change())
}

async fn handle_delete(g: Arc<OnionService>, ctx: Arc<Mutex<Context>>) -> Result<Action, Error> {
    let Some(ns) = g.metadata.namespace.as_ref() else {
        return Err(Error::Api(ErrorResponse {
            status: "".to_string(),
            message: "".to_string(),
            reason: "".to_string(),
            code: 0,
        }));
    };
    let Some(name) = g.metadata.name.as_ref() else {
        return Err(Error::Api(ErrorResponse {
            status: "".to_string(),
            message: "".to_string(),
            reason: "".to_string(),
            code: 0,
        }));
    };
    let mut ctx = ctx.lock().await;
    if let Some(svc) = ctx.running_svcs.get_mut(ns).and_then(|m| m.remove(name)) {
        svc.abort_handle.abort();
    }
    Ok(Action::await_change())
}

async fn reconcile(
    obj: Arc<OnionService>,
    ctx: Arc<Mutex<Context>>,
) -> Result<Action, kube::runtime::finalizer::Error<Error>> {
    let ns = obj.meta().namespace.as_deref().unwrap();
    let client = {
        let ctx = ctx.lock().await;
        let client = ctx.client.clone();
        client
    };
    let onion_svc: Api<OnionService> = Api::namespaced(client, ns);
    finalizer(&onion_svc, "arti.nirvati.org/cleanup", obj, |event| async {
        match event {
            Event::Apply(onion_svc) => {
                tracing::debug!("Handling update");
                tokio::spawn(handle_update(onion_svc, ctx))
                    .await
                    .unwrap_or_else(|e| {
                        tracing::error!("Error while handling update: {:?}", e);
                        Ok(Action::requeue(Duration::from_secs(60)))
                    })
            }
            Event::Cleanup(onion_svc) => {
                tracing::debug!("Handling delete");
                handle_delete(onion_svc, ctx).await
            }
        }
    })
    .await
}

fn error_policy(
    _obj: Arc<OnionService>,
    _error: &kube::runtime::finalizer::Error<Error>,
    _ctx: Arc<Mutex<Context>>,
) -> Action {
    Action::requeue(Duration::from_secs(60))
}

pub async fn start_controller() -> Result<(), Box<dyn std::error::Error>> {
    // Init Tor
    let (dirmgr, circpool) = crate::tor::bootstrap().await.unwrap();

    // Initialize Kubernetes
    let client = Client::try_default().await?;

    tracing::debug!("Obtained keystore!");
    // Create a new directory in /tmp to store the onion service state
    let state_dir = std::path::PathBuf::from("/tmp/arti-state");
    let context = Arc::new(Mutex::new(Context {
        client: client.clone(),
        running_svcs: Default::default(),
        dirmgr,
        circpool,
        state_dir,
    }));
    let onion_svcs = Api::<OnionService>::all(client.clone());
    let secrets = Api::<Secret>::all(client);
    Controller::new(onion_svcs, watcher::Config::default())
        .owns(secrets, watcher::Config::default())
        .run(reconcile, error_policy, context)
        .for_each(|res| async move {
            match res {
                Ok(o) => tracing::debug!("reconciled {:?}", o),
                Err(e) => tracing::error!("reconcile failed: {:?}", e),
            }
        })
        .await;
    Ok(())
}
