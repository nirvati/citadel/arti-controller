use base64::{engine::general_purpose::STANDARD_NO_PAD, Engine as _};
use k8s_openapi::api::core::v1::Secret;
use k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use k8s_openapi::ByteString;
use std::path::PathBuf;
use std::str::FromStr;
use tor_error::{Bug, ErrorKind};
use tor_keymgr::{
    ArtiPath, EncodableKey, ErasedKey, KeyPath, KeySpecifier, KeyType, Keystore, KeystoreId,
    SshKeyData,
};
use ureq::tls::{Certificate, RootCerts, TlsProvider};

pub struct K8sSecretKeyStore {
    token: String,
    kube_base_url: String,
    ureq_agent: ureq::Agent,
    pub namespace: String,
    pub secret_name: String,
    keystore_id: KeystoreId,
}

impl K8sSecretKeyStore {
    pub fn new(namespace: String, secret_name: String) -> anyhow::Result<Self> {
        let keystore_id =
            KeystoreId::from_str(&format!("k8s-secret-{}-{}", namespace, secret_name))
                .expect("Failed to create KeystoreId");
        // Read token from /run/secrets/kubernetes.io/serviceaccount/token
        let token = std::fs::read_to_string("/run/secrets/kubernetes.io/serviceaccount/token")
            .expect("Failed to read token");
        let ca_cert = std::fs::read("/var/run/secrets/kubernetes.io/serviceaccount/ca.crt")?;
        let parsed_cert = Certificate::from_pem(&ca_cert)?.to_owned();
        let root_certs = RootCerts::new_with_certs(&[parsed_cert]);
        let tls_config = ureq::tls::TlsConfig::builder().provider(TlsProvider::NativeTls).root_certs(root_certs).disable_verification(true).build();

        let kube_host = std::env::var("KUBERNETES_SERVICE_HOST")
            .expect("Failed to get KUBERNETES_SERVICE_HOST");
        let kube_port = std::env::var("KUBERNETES_SERVICE_PORT")
            .expect("Failed to get KUBERNETES_SERVICE_PORT");
        let kube_base_url = format!("https://{}:{}", kube_host, kube_port);
        let agent_config = ureq::Agent::config_builder()
            .tls_config(tls_config)
            .https_only(true)
            .build();
        let tmp_store = K8sSecretKeyStore {
            token,
            namespace,
            secret_name,
            keystore_id,
            kube_base_url,
            ureq_agent: agent_config.into(),
        };
        let ns = tmp_store.namespace.clone();
        let secret_name = tmp_store.secret_name.clone();
        let resp = tmp_store
            .ureq_agent
            .get(&format!(
                "{}/api/v1/namespaces/{}/secrets/{}",
                tmp_store.kube_base_url, ns, secret_name
            ))
            .header("Authorization", &format!("Bearer {}", tmp_store.token))
            .call();
        if let Err(ureq::Error::StatusCode(404)) = resp {
            let secret = Secret {
                metadata: ObjectMeta {
                    name: Some(secret_name.clone()),
                    namespace: Some(ns.clone()),
                    ..Default::default()
                },
                data: None,
                string_data: None,
                type_: None,
                immutable: None,
            };
            tmp_store
                .ureq_agent
                .post(&format!(
                    "{}/api/v1/namespaces/{}/secrets",
                    tmp_store.kube_base_url, ns
                ))
                .header("Authorization", &format!("Bearer {}", tmp_store.token))
                .send_json(secret)?;
        } else if let Err(e) = resp {
            return Err(anyhow::anyhow!("Failed to get secret: {}", e));
        }
        Ok(tmp_store)
    }

    fn get_secret(&self) -> tor_keymgr::Result<Secret> {
        let resp = self
            .ureq_agent
            .get(&format!(
                "{}/api/v1/namespaces/{}/secrets/{}",
                self.kube_base_url, self.namespace, self.secret_name
            ))
            .header("Authorization", &format!("Bearer {}", self.token))
            .call()
            .map_err(|e| {
                tor_keymgr::Error::Bug(Bug::new(
                    ErrorKind::Internal,
                    format!("Failed to get secret: {}", e),
                ))
            })?;
        let secret: Secret = resp.into_body().read_json().map_err(|e| {
            tor_keymgr::Error::Bug(Bug::new(
                ErrorKind::Internal,
                format!("Failed to parse secret: {}", e),
            ))
        })?;
        Ok(secret)
    }

    fn patch_secret(&self, secret: Secret) -> tor_keymgr::Result<()> {
        self.ureq_agent
            .patch(&format!(
                "{}/api/v1/namespaces/{}/secrets/{}",
                self.kube_base_url, self.namespace, self.secret_name
            ))
            .header("Authorization", &format!("Bearer {}", self.token))
            .header("Content-Type", "application/strategic-merge-patch+json")
            .send_json(secret)
            .map_err(|e| {
                tor_keymgr::Error::Bug(Bug::new(
                    ErrorKind::Internal,
                    if let ureq::Error::StatusCode(code) = e {
                        format!("Failed to patch secret: {}", code)
                    } else {
                        format!("Failed to patch secret: {}", e)
                    },
                ))
            })?;
        Ok(())
    }

    fn do_insert(
        &self,
        key: &dyn EncodableKey,
        key_spec: &dyn KeySpecifier,
        key_type: &KeyType,
    ) -> tor_keymgr::Result<()> {
        tracing::debug!("Inserting key into K8sSecretKeyStore");
        let arti_path = key_spec.arti_path().unwrap();
        let key_data = key.as_ssh_key_data()?.to_openssh_string("")?;
        let arti_extension = key_type.arti_extension();
        let mut secret = self.get_secret()?;
        if secret.data.is_none() {
            secret.data = Some(Default::default());
        }
        let data = secret.data.as_mut().unwrap();
        let key_name = base64_encode(format!("{}:{}", arti_extension, arti_path).as_bytes());
        data.insert(key_name, ByteString(key_data.as_bytes().to_vec()));
        secret.metadata.managed_fields = None;
        self.patch_secret(secret)?;
        Ok(())
    }

    fn do_remove(
        &self,
        key_spec: &dyn KeySpecifier,
        key_type: &KeyType,
    ) -> tor_keymgr::Result<Option<()>> {
        tracing::debug!("Removing key from K8sSecretKeyStore");
        let arti_path = key_spec.arti_path().unwrap();
        let arti_extension = key_type.arti_extension();
        let mut secret = self.get_secret()?;
        if secret.data.is_none() {
            return Ok(None);
        }
        let data = secret.data.as_mut().unwrap();
        let key_name = base64_encode(format!("{}:{}", arti_extension, arti_path).as_bytes());
        let removed = data.remove(&key_name);
        if removed.is_none() {
            return Ok(None);
        }
        secret.metadata.managed_fields = None;
        self.patch_secret(secret)?;
        Ok(Some(()))
    }
}

fn base64_encode(data: &[u8]) -> String {
    STANDARD_NO_PAD.encode(data)
}

fn base64_decode(data: &str) -> Vec<u8> {
    STANDARD_NO_PAD.decode(data).unwrap()
}

impl Keystore for K8sSecretKeyStore {
    fn id(&self) -> &KeystoreId {
        return &self.keystore_id;
    }

    fn contains(
        &self,
        key_spec: &dyn KeySpecifier,
        key_type: &KeyType,
    ) -> tor_keymgr::Result<bool> {
        tracing::debug!("K8sSecretKeyStore::contains");
        let secret = self.get_secret()?;
        tracing::debug!("Obtained secret");
        let data = secret.data.unwrap_or_default();
        let key = base64_encode(
            format!(
                "{}:{}",
                key_type.arti_extension(),
                key_spec.arti_path().unwrap(),
            )
            .as_bytes(),
        );
        Ok(data.contains_key(&key))
    }

    fn get(
        &self,
        key_spec: &dyn KeySpecifier,
        key_type: &KeyType,
    ) -> tor_keymgr::Result<Option<ErasedKey>> {
        tracing::debug!("K8sSecretKeyStore::get");
        let secret = self.get_secret()?;
        tracing::debug!("Obtained secret");
        let data = secret.data.unwrap_or_default();
        let key = base64_encode(
            format!(
                "{}:{}",
                key_type.arti_extension(),
                key_spec.arti_path().unwrap()
            )
            .as_bytes(),
        );
        let key_data = data.get(&key);
        let arti_path: String = key_spec
            .arti_path()
            .map_err(|e| {
                tor_keymgr::Error::Bug(Bug::new(
                    ErrorKind::Internal,
                    format!("Failed to get Arti path: {}", e),
                ))
            })?
            .into();
        let mut rel_path = PathBuf::from(arti_path);
        rel_path.set_extension(key_type.arti_extension());
        match key_data {
            Some(data) => {
                Ok(Some(match key_type {
                    KeyType::Ed25519Keypair | KeyType::X25519StaticKeypair | KeyType::Ed25519ExpandedKeypair => {
                        let privkey = ssh_key::PrivateKey::from_openssh(&data.0).map_err(|err| {
                            tor_keymgr::Error::Bug(Bug::new(
                                ErrorKind::Internal,
                                format!("Failed to parse private key: {}", err),
                            ))
                        })?;
                        SshKeyData::try_from_keypair_data(privkey.key_data().clone())?.into_erased()?
                    }
                    KeyType::Ed25519PublicKey | KeyType::X25519PublicKey => {
                        let pubkey_str = std::str::from_utf8(&data.0).map_err(|err| {
                            tor_keymgr::Error::Bug(Bug::new(
                                ErrorKind::Internal,
                                format!("Failed to parse public key: {}", err),
                            ))
                        })?;
                        let pubkey = ssh_key::PublicKey::from_openssh(pubkey_str).map_err(|err| {
                            tor_keymgr::Error::Bug(Bug::new(
                                ErrorKind::Internal,
                                format!("Failed to parse public key: {}", err),
                            ))
                        })?;
                        SshKeyData::try_from_key_data(pubkey.key_data().clone())?.into_erased()?
                    }
                    KeyType::Unknown { .. } | _ => {
                        return Err(tor_keymgr::Error::Bug(Bug::new(
                            ErrorKind::Internal,
                            format!("Unsupported key type: {:?}", key_type),
                        )));
                    }
                }))
            },
            None => Ok(None),
        }
    }

    fn insert(
        &self,
        key: &dyn EncodableKey,
        key_spec: &dyn KeySpecifier,
        key_type: &KeyType,
    ) -> tor_keymgr::Result<()> {
        let mut tries = 0;
        loop {
            match self.do_insert(key, key_spec, key_type) {
                Ok(_) => return Ok(()),
                Err(e) => {
                    if tries >= 3 {
                        return Err(e);
                    }
                    tracing::error!("Failed to insert key: {}", e);
                    tries += 1;
                    std::thread::sleep(std::time::Duration::from_secs(1));
                }
            }
        }
    }

    fn remove(
        &self,
        key_spec: &dyn KeySpecifier,
        key_type: &KeyType,
    ) -> tor_keymgr::Result<Option<()>> {
        let mut tries = 0;
        loop {
            match self.do_remove(key_spec, key_type) {
                Ok(res) => return Ok(res),
                Err(e) => {
                    if tries >= 3 {
                        return Err(e);
                    }
                    tries += 1;
                    std::thread::sleep(std::time::Duration::from_secs(1));
                }
            }
        }
    }

    fn list(&self) -> tor_keymgr::Result<Vec<(KeyPath, KeyType)>> {
        tracing::debug!("Listing keys in K8sSecretKeyStore");
        let secret = self.get_secret()?;
        let data = secret.data.unwrap_or_default();
        // Each key is <key_type>:<key_path>
        let keys = data
            .keys()
            .map(|key| {
                let key = String::from_utf8(base64_decode(key)).expect("Failed to decode key");
                let key_parts = key.split(':').collect::<Vec<&str>>();
                let key_type = KeyType::from(key_parts[0]);
                let key_path = key_parts[1];
                let key_path = KeyPath::Arti(ArtiPath::new(key_path.to_string()).unwrap());
                (key_path, key_type)
            })
            .collect();
        Ok(keys)
    }
}
