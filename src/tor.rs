use fs_mistrust::Mistrust;
use std::path::PathBuf;
use std::sync::Arc;
use tor_chanmgr::{ChannelConfig, Dormancy};
use tor_circmgr::hspool::HsCircPool;
use tor_circmgr::{CircMgrConfig, CircuitTiming, PathConfig, PreemptiveCircuitConfig};
use tor_config::ExplicitOrAuto;
use tor_dirmgr::{DirMgr, DirMgrStore, DirProvider};
use tor_guardmgr::bridge::BridgeConfig;
use tor_guardmgr::fallback::{FallbackList, FallbackListBuilder};
use tor_guardmgr::{VanguardConfig, VanguardConfigBuilder, VanguardMode};
use tor_netdir::params::NetParameters;
use tor_netdir::UpcastArcNetDirProvider;
use tor_persist::{FsStateMgr, StateMgr};
use tor_proto::tor_memquota::{Config, MemoryQuotaTracker};
use tor_rtcompat::PreferredRuntime;

struct TorConfig {
    path_config: PathConfig,
    circuit_timing: CircuitTiming,
    preemptive_circuit_config: PreemptiveCircuitConfig,
    fallback_list: FallbackList,
    vanguard_config: VanguardConfig,
}

impl AsRef<FallbackList> for TorConfig {
    fn as_ref(&self) -> &FallbackList {
        &self.fallback_list
    }
}

impl AsRef<[BridgeConfig]> for TorConfig {
    fn as_ref(&self) -> &[BridgeConfig] {
        &[]
    }
}

impl tor_guardmgr::GuardMgrConfig for TorConfig {
    fn bridges_enabled(&self) -> bool {
        false
    }
}

impl AsRef<PathConfig> for TorConfig {
    fn as_ref(&self) -> &PathConfig {
        &self.path_config
    }
}

impl AsRef<CircuitTiming> for TorConfig {
    fn as_ref(&self) -> &CircuitTiming {
        &self.circuit_timing
    }
}

impl AsRef<PreemptiveCircuitConfig> for TorConfig {
    fn as_ref(&self) -> &PreemptiveCircuitConfig {
        &self.preemptive_circuit_config
    }
}

impl CircMgrConfig for TorConfig {
    fn vanguard_config(&self) -> &VanguardConfig {
        &self.vanguard_config
    }
}

pub async fn bootstrap() -> Result<
    (
        Arc<DirMgr<PreferredRuntime>>,
        Arc<HsCircPool<PreferredRuntime>>,
    ),
    Box<dyn std::error::Error>,
> {
    let dir_mgr_cache = std::path::PathBuf::from("/tmp/dirmgr-cache");
    let state_mgr_dir = std::path::PathBuf::from("/tmp/state-mgr");
    let runtime = PreferredRuntime::current()?;

    let quota_tracker = MemoryQuotaTracker::new(&runtime, Config::builder().build().unwrap())?;

    let statemgr = FsStateMgr::from_path_and_mistrust(
        &state_mgr_dir,
        &Mistrust::new_dangerously_trust_everyone(),
    )?;
    // Try to take state ownership early, so we'll know if we have it.
    // (At this point we don't yet care if we have it.)
    let _ignore_status = statemgr.try_lock()?;

    let channel_config = ChannelConfig::builder().build().unwrap();
    let chanmgr = Arc::new(tor_chanmgr::ChanMgr::new(
        runtime.clone(),
        &channel_config,
        Dormancy::Active,
        &NetParameters::default(),
        quota_tracker,
    ));
    let guardmgr = tor_guardmgr::GuardMgr::new(
        runtime.clone(),
        statemgr.clone(),
        &TorConfig {
            path_config: Default::default(),
            circuit_timing: Default::default(),
            preemptive_circuit_config: Default::default(),
            fallback_list: FallbackListBuilder::default().build().unwrap(),
            // TODO: Make this configurable
            vanguard_config: VanguardConfigBuilder::default().mode(ExplicitOrAuto::Explicit(VanguardMode::Full)).build().unwrap(),
        },
    )?;

    let circmgr = Arc::new(tor_circmgr::CircMgr::new(
        &TorConfig {
            path_config: Default::default(),
            circuit_timing: Default::default(),
            preemptive_circuit_config: Default::default(),
            fallback_list: FallbackListBuilder::default().build().unwrap(),
            // TODO: Make this configurable
            vanguard_config: VanguardConfigBuilder::default().mode(ExplicitOrAuto::Explicit(VanguardMode::Full)).build().unwrap(),
        },
        statemgr.clone(),
        &runtime,
        Arc::clone(&chanmgr),
        &guardmgr,
    )?);

    let mut dir_cfg = tor_dirmgr::DirMgrConfig {
        cache_dir: PathBuf::from("/tmp/dirmgr-cache"),
        cache_trust: Mistrust::new_dangerously_trust_everyone(),
        network: Default::default(),
        schedule: Default::default(),
        tolerance: Default::default(),
        override_net_params: Default::default(),
        extensions: Default::default(),
    };
    dir_cfg.cache_dir = dir_mgr_cache;
    let dirmgr_store = DirMgrStore::new(&dir_cfg, runtime.clone(), false)?;
    let dirmgr =
        DirMgr::create_unbootstrapped(dir_cfg, runtime.clone(), dirmgr_store, Arc::clone(&circmgr))
            .unwrap();

    let mut periodic_task_handles =
        circmgr.launch_background_tasks(&runtime, &dirmgr, statemgr.clone())?;
    periodic_task_handles.extend(dirmgr.download_task_handle());

    periodic_task_handles
        .extend(chanmgr.launch_background_tasks(&runtime, dirmgr.clone().upcast_arc())?);

    let hs_circ_pool = {
        let circpool = Arc::new(HsCircPool::new(&circmgr));
        circpool.launch_background_tasks(&runtime, &dirmgr.clone().upcast_arc())?;
        circpool
    };

    dirmgr
        .bootstrap()
        .await
        .expect("Failed to bootstrap dirmgr");

    Ok((dirmgr, hs_circ_pool))
}
