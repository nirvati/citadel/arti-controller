use chrono::{DateTime, Utc};
use kube::CustomResource;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(CustomResource, Deserialize, Serialize, Clone, Debug, JsonSchema)]
#[kube(
    kind = "OnionService",
    group = "arti.nirvati.org",
    version = "v1alpha1",
    namespaced
)]
#[kube(status = "OnionServiceStatus")]
pub struct OnionServiceSpec {
    pub routes: Vec<OnionServiceRoute>,
    pub key_secret: SecretReference,
    /*#[serde(default)]
    pub insecure_single_hop: bool,*/
    pub store_name_in_secret: Option<NameSecretReference>,
}

#[derive(Deserialize, Serialize, Clone, Debug, JsonSchema)]
pub struct SecretReference {
    /// name is unique within a namespace to reference a secret resource.
    pub name: String,
    /// namespace defines the space within which the secret name must be unique.
    pub namespace: Option<String>,
}


#[derive(Deserialize, Serialize, Clone, Debug, JsonSchema)]
pub struct NameSecretReference {
    /// name is unique within a namespace to reference a secret resource.
    pub name: String,
    /// namespace defines the space within which the secret name must be unique.
    pub namespace: Option<String>,
    /// Key is the name of the key in the secret's data field to store the service name.
    pub key: String,
}

#[derive(Deserialize, Serialize, Clone, Debug, JsonSchema, PartialEq)]
pub struct OnionServiceRoute {
    /// Any connections to a port matching this pattern match this rule.
    pub(crate) source_port: u16,
    /// When this rule matches, we take this action.
    pub target: OnionServiceTarget,
}

#[derive(Deserialize, Serialize, Clone, Debug, JsonSchema)]
pub struct PortRange {
    pub start: u16,
    pub end: u16,
}

#[derive(Deserialize, Serialize, Clone, Debug, JsonSchema, PartialEq)]
pub struct OnionServiceTarget {
    pub port: u16,
    #[serde(flatten)]
    pub host: OnionServiceTargetHost,
}

#[derive(Deserialize, Serialize, Clone, Debug, JsonSchema, PartialEq)]
#[serde(untagged)]
pub enum OnionServiceTargetHost {
    Service(OnionServiceTargetHostService),
    DnsName(OnionServiceTargetHostDnsName),
    Ip(OnionServiceTargetHostIp),
}

#[derive(Deserialize, Serialize, Clone, Debug, JsonSchema, PartialEq)]
pub struct OnionServiceTargetHostService {
    pub svc: String,
    pub ns: Option<String>,
}

#[derive(Deserialize, Serialize, Clone, Debug, JsonSchema, PartialEq)]
pub struct OnionServiceTargetHostDnsName {
    pub dns_name: String,
}

#[derive(Deserialize, Serialize, Clone, Debug, JsonSchema, PartialEq)]
pub struct OnionServiceTargetHostIp {
    pub ip: String,
}

#[derive(Deserialize, Serialize, Clone, Debug, JsonSchema)]
pub enum State {
    Shutdown,
    Bootstrapping,
    Degraded,
    Running,
    Recovering,
    Broken,
}

#[derive(Deserialize, Serialize, Clone, Debug, JsonSchema)]
pub struct Condition {
    state: State,
    message: Option<String>,
    time: DateTime<Utc>,
}

#[derive(Deserialize, Serialize, Clone, Debug, JsonSchema)]
pub struct OnionServiceStatus {
    pub onion_name: String,
    pub conditions: Vec<Condition>,
}
